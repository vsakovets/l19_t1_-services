package com.svs.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class LogWriterService extends IntentService {

    public static final String EXTRA_MESSAGE = "message";

    public static Intent newIntent(Context context) {
        return new Intent(context, LogWriterService.class);
    }

    public LogWriterService() {
        super("LogWriterService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String text = intent.getStringExtra(EXTRA_MESSAGE);
        IOUtils.saveLogToFile(this, text);
        showText(text);
    }

    private void showText(String text) {
        Log.v("LogWriterService", "Pressed \"" + text + "\"");
    }
}

