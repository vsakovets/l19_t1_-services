package com.svs.services;

import android.content.Context;
import android.os.Environment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class IOUtils {


    private static final String FILE_NAME = "/logdata.txt";

    public static void saveLogToFile(Context context, final String text) {


        File file = new File(Environment.getExternalStorageDirectory().toString() + FILE_NAME);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try (FileWriter fileWriter = new FileWriter(new File(Environment.getExternalStorageDirectory().toString() + FILE_NAME), true)) {
            fileWriter.write("Pressed \"" + text + "\"\n");
            fileWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static StringBuilder readLogs() {
        StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new FileReader(
                new File(Environment.getExternalStorageDirectory().toString() + FILE_NAME)))) {
            String newLine = null;
            while ((newLine = reader.readLine()) != null) {
                stringBuilder.append(newLine + "\n");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringBuilder;

    }

}