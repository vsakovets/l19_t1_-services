package com.svs.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import androidx.annotation.Nullable;

public class LogReaderService extends Service {
    private final IBinder binder = new LogReaderBinder();

    public class LogReaderBinder extends Binder {
        LogReaderService getLogReader() {
            return LogReaderService.this;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public StringBuilder getLogs() {
        return IOUtils.readLogs();
    }

}
